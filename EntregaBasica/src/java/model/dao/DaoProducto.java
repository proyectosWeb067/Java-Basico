/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import dto.Producto;

/**
 *
 * @author juan
 */
public interface DaoProducto {
    
    public List<Producto> getProductos();
    
    public String insertProducto(Producto p);
    
    public String updateProducto(Producto p); 
    
     public String deleteProducto(Integer idproducto);
     
      public Producto getProducto(Integer idproducto);
  
    
}
