/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.daoimpl;

import conexion.Conexion;
import dto.Producto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.dao.DaoProducto;

/**
 *
 * @author juan
 */
public class DaoProductoImpl implements DaoProducto {

    private Conexion conectar;

    public DaoProductoImpl() {
        this.conectar = new Conexion();
    }

    @Override
    public List<Producto> getProductos() {
        List<Producto> lista = null;
        Connection cn = conectar.connection();
        String sql = "SELECT "
                + "product_id,"
                + "code,"
                + "name,"
                + "description,"
                + "price "
                + "FROM product "
                + "ORDER BY product_id ASC ";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            lista = new ArrayList();

            while (rs.next()) {
                Producto pro = new Producto();

                pro.setProduct_id(rs.getInt(1));
                pro.setCode(rs.getString(2));
                pro.setName(rs.getString(3));
                pro.setDescription(rs.getString(4));
                pro.setPrice(rs.getDouble(5));

                lista.add(pro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoProductoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                
            }
        }
        return lista;
    }

    @Override
    public String insertProducto(Producto p) {
        String result = null;
        Connection cn = conectar.connection();
        String sql = "INSERT INTO product (code, name, price, description) values(?,?,?,?)";
       
        try {
            PreparedStatement prs = cn.prepareStatement(sql);
        
            prs.setString(1, p.getCode());
            prs.setString(2, p.getName());
            prs.setDouble(3, p.getPrice());
            prs.setString(4, p.getDescription());
            int registro = prs.executeUpdate();
            if (registro == 0) {
                result = "0 filas afectadas";
            }
        } catch (SQLException ex) {
         }finally{
            try {
                cn.close();
            } catch (SQLException ex) {
                 result = ex.getMessage();
            }
        }   
        return result;
    }

    @Override
    public String updateProducto(Producto p) {
        String result = null;
        Connection cn = conectar.connection();
        String sql = "UPDATE product SET code = ? , name = ?, price = ?, description = ? WHERE product_id = ?";
        
        try {
            PreparedStatement ps = cn.prepareStatement(sql);
            ps.setString(1, p.getCode());
            ps.setString(2, p.getName());
            ps.setDouble(3, p.getPrice());
            ps.setString(4, p.getDescription());
            ps.setInt(5, p.getProduct_id());
            
            int update = ps.executeUpdate();
            if(update == 0){
                result = "no se pudo actualizar";
            }
            
            
        } catch (SQLException ex) {
             
        }
        
        
        return result;
    }

    @Override
    public String deleteProducto(Integer idproducto) {
        String result = null;
        Connection cn = conectar.connection();
        String sql = "DELETE FROM product WHERE  product_id = ? ";
        
        try {
            PreparedStatement ps = cn.prepareStatement(sql);
            ps.setInt(1, idproducto);
            
            int del = ps.executeUpdate();
            if(del == 0){
           
                result = "No se elimino el registro";
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoProductoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return result;

    }

    @Override
    public Producto getProducto(Integer idproducto) {
            Producto p = null;
            Connection cn = conectar.connection();
            String sql = "SELECT "
                + "product_id,"    
                + "code,"
                + "name,"
                + "description,"
                + "price "
                + "FROM product "
                + "WHERE product_id = ? ";
            
        try {
            PreparedStatement ps = cn.prepareStatement(sql);
            ps.setInt(1, idproducto);
            ResultSet rs = ps.executeQuery();
            
            
            while(rs.next()){
                p = new Producto();
                p.setProduct_id(rs.getInt(1));
                p.setCode(rs.getString(2));
                p.setName(rs.getString(3));
                p.setDescription(rs.getString(4));
                p.setPrice(rs.getDouble(5));
            }
            
        } catch (SQLException ex) {
            
        }finally{
                try {
                    cn.close();
                } catch (SQLException ex) {
                
                }
        }
        return p;
    }

}
