package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
 
import java.sql.SQLException;
 
public class Conexion {

    
    private String db;

    public Conexion() {
        db = "dbproduct"; // base de datos a conectarse
    }

    public Connection connection() {
        Connection cn = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            cn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/" + db, "root", "");

        } catch (SQLException e) {
        } catch (Exception e) {
        }

        return cn;
    }
}
