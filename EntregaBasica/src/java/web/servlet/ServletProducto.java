package web.servlet;

import dto.Producto;
import java.io.IOException;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.DaoProducto;
import model.daoimpl.DaoProductoImpl;

@WebServlet(name = "ServletProducto", urlPatterns = {"/Producto"})
public class ServletProducto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        // ---parametros eliminar
        String accion = request.getParameter("accion");
        String id = request.getParameter("id");
        request.getSession().setAttribute("id", id);
        String nom = request.getParameter("nombre");
        request.getSession().setAttribute("nombre", nom);
        String res = null;
        String tar = null;
        // ---

        DaoProducto daoPro = new DaoProductoImpl();

        if (accion == null) {
            res = "Solicitud no recibida";

        } else if (accion.equals("OK")) {
            List<Producto> lista = daoPro.getProductos();

            if (lista != null) {
                request.getSession().setAttribute("lista", lista);
                tar = "view/Producto/";

            } else {
                res = "Problemas en Consulta";
            }
        } else if (accion.equals("INS")) {
            Producto p = new Producto();
            res = validacion(p, request);

            if (res == null) {
                res = daoPro.insertProducto(p);
            }
            if (res == null) {
                List<Producto> lista = daoPro.getProductos();
                request.getSession().setAttribute("lista", lista);
                tar = "view/Producto/";
            }
        } else if (accion.equals("DEL") || accion.equals("DELOK")) {

            tar = "view/Producto/delproducto.jsp";

            if (tar.equals("view/Producto/delproducto.jsp") && accion.equals("DELOK")) {
                daoPro.deleteProducto(Integer.valueOf(id));
                if (res == null) {
                    List<Producto> lista = daoPro.getProductos();
                    request.getSession().setAttribute("lista", lista);

                    tar = "view/Producto/";

                }

            }

        } else if (accion.equals("GET")) {
            String idp = request.getParameter("id");
            Producto p = daoPro.getProducto(Integer.valueOf(idp));

            if (p != null) {
                request.getSession().setAttribute("producto", p);
                tar = "view/Producto/updproducto.jsp";
            } else {
                res = "Problemas en obtener datos de Profesor";
            }

        } else if (accion.equals("UPD")) {
            Producto p = new Producto();
            res = validacion(p, request);

            if (res == null) {
                daoPro.updateProducto(p);
            }

            if (res == null) {
                List<Producto> lista = daoPro.getProductos();
                request.getSession().setAttribute("lista", lista);
                tar = "view/Producto/index.jsp";
            }
        } else if (accion.equals("READ")) {
            String idp = request.getParameter("id");
            Producto p = daoPro.getProducto(Integer.valueOf(idp));

            if (p != null) {
                request.getSession().setAttribute("producto", p);
                tar = "view/Producto/readproducto.jsp";
            } else {
                res = "Problemas en obtener datos de Profesor";
            }

        } else {
            res = "Solicitud no reconocida";
        }
        if (res != null) {
            request.getSession().setAttribute("msg", res);
            tar = "mensajes.jsp";
        }

        if (tar != null) {
            response.sendRedirect(tar);
        }

    }

    private String validacion(Producto p, HttpServletRequest request) {

        String msg;

        String product_id = request.getParameter("product_id");
        String codigo = request.getParameter("codigo");
        String nombre = request.getParameter("nombre");
        String precio = request.getParameter("precio");
        String description = request.getParameter("description");

        msg = ((codigo == null) || (codigo.trim().isEmpty()))
                ? "&iexcl;Ingrese El Codigo!" : null;

        if (msg == null) {
            msg = ((nombre == null) || (nombre.trim().isEmpty()))
                    ? "&iexcl;Ingrese El Producto!" : null;
        }

        if (msg == null) {
            msg = ((precio == null) || (precio.trim().isEmpty()))
                    ? "&iexcl;Ingrese Un Precio!" : null;
        }
        if (msg == null) {
            msg = ((description == null) || (description.trim().isEmpty()))
                    ? "&iexcl;Ingrese Una Descripcion!" : null;
        }

        if (msg == null) {
            if (product_id == null) {
                p.setProduct_id(null);
            } else {
                p.setProduct_id(Integer.valueOf(product_id));
            }

            p.setCode(codigo);
            p.setName(nombre);
            p.setPrice(Double.parseDouble(precio));
            p.setDescription(description);
        }

        return msg;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
