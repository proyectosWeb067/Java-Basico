<%-- 
    Document   : readproducto
    Created on : 7/04/2018, 03:38:33 PM
    Author     : juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link   href="../../css/bootstrap.min.css" rel="stylesheet"/>
        
      
		    <title>JAVA EE</title>
    </head>
    <body>
         <body>
        <div class="container">
            <div class="row">
                <h3>JAVA BASICO - CRUD</h3>
            </div>
            <div class="row">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="" data-toggle="tab">Productos</a></li>                        
                    </ul>                    
                </div>
            </div>    
            <div class="row">
                <h3>Mostrar producto</h3>
            </div>
            <div class="row">
                <form class="form-horizontal" action="../../Producto" method="post">
                     <input type="hidden" name="accion" value="READ"/>
                     <input type="hidden" name="product_id" 
                       value="${producto.product_id}"/>
                    <div class="control-group">
                        <label class="control-label" style="font-weight: bold;">CODIGO</label>
                        <div class="controls">
                            <label class="checkbox">
                                ${producto.code}
                            </label>                  
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" style="font-weight: bold;">NOMBRE</label>
                        <div class="controls">
                            <label class="checkbox">
                                ${producto.name}   
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"style="font-weight: bold;">PRECIO</label>
                        <div class="controls">
                            <label class="checkbox">
                                 ${producto.price}   
                            </label>                         
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" style="font-weight: bold;">DESCRIPCION</label>
                        <div class="controls">
                            <label class="checkbox">
                                 ${producto.description}            
                            </label>              
                        </div>
                    </div>
                    <div class="form-actions">
                        
                        <a class="btn" href="../../Producto?accion=OK">Cancelar</a>
                      
                                               
                    </div>
                </form>
            </div>
        </div>

    </body>

    </body>
</html>