<%-- 
    Document   : Producto
    Created on : 3/04/2018, 06:02:04 PM
    Author     : juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
        <link href="../../css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
       
        <title>JSP Page</title>
    </head>
    <body>
        <section class="container">
            <h1>Java Basico</h1>
        <div class="tabbable">
            <ul class="nav nav-tabs">
                <li class="active"><a href="" data-toggle="tab">Productos</a></li>
                
            </ul>                    
        </div>
            <div>
                
                <a style="float: right; margin: 5px;"href="insproducto.jsp" class="btn btn-success" >Agregar</a>
            </div>    
        <table class="table table-striped table-bordered">
            <thead>
            <th>ID</th>
            <th>CODIGO</th>
            <th>PRODUCTO</th>
            <th>DESCRIPCION</th>
            <th>PRECIO</th>  
            <th style="text-align: center;">ACCIONES</th>
        </thead>
        <tbody >
            <c:forEach var="prf" items="${lista}">
                <tr>
                    <td>${prf.product_id}</td>
                    <td>${prf.code}</td>
                    <td>${prf.name}</td>
                    <td>${prf.description}</td>
                    <td>${prf.price}</td>

                    <td><a class="btn" 
                           href="../../Producto?accion=READ&id=${prf.product_id}"/>Detalle</a> 

                        <a class="btn btn-success"
                           href="../../Producto?accion=GET&id=${prf.product_id}"/>Editar</a> 

                        <a class="btn btn-danger"
                           href="../../Producto?accion=DEL&id=${prf.product_id}&nombre=${prf.name}"/>Eliminar</a></td>
                           
                </tr>        
            </c:forEach>
        </tbody>
     </table>
    </section>
 </body>
</html>

