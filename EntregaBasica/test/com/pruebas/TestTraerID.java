/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebas;

import dto.Producto;
import model.dao.DaoProducto;
import model.daoimpl.DaoProductoImpl;
 
/**
 *
 * @author juan
 */
public class TestTraerID {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DaoProducto pr = new DaoProductoImpl();
        Producto p = new Producto();
        p = pr.getProducto(2);
        
        System.out.println("producto que trae por id 1 -> " + p.getName());
        
    }
    
}
