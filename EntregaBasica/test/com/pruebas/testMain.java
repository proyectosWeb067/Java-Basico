/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebas;

import dto.Producto;
import java.util.*;
import model.dao.DaoProducto;
import model.daoimpl.DaoProductoImpl;

/**
 *
 * @author juan
 */
public class testMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DaoProducto dap = new DaoProductoImpl();
        List<Producto> lista = new ArrayList();
        lista = dap.getProductos();
        
        for (Producto producto : lista) {
            System.out.println("Productos disponibles en base -> " + producto.getName());
        }
    }
    
}
